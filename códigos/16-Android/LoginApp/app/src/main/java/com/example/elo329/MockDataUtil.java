package com.example.elo329;

import com.example.elo329.Models.UserModel;

public class MockDataUtil {
    public static UserModel createMockUser() {
        return new UserModel("fcabezas@gmail.com", "123456");
    }
}
